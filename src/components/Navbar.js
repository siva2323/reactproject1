import React from "react"

export default function Navbar() {
    return (
        <div className="titleBox">
            <img src={require('./logo.png')} className="imgLogo" />
            <h3 className="mainTitle">ReactFacts</h3>
            <h4 className="sideTitle">React Course - Project 1</h4>
        </div>
    )
}