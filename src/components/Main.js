import React from "react"

export default function Main()
{
    return ( 
    <main className="contentBox">
        <h1 className="contentTitle">Fun facts about React</h1>
        <ul className="contentList">
            <li className="classLi">Was first released in 2013</li>
            <li className="classLi">Was originally created by Jordan Walke</li>
            <li className="classLi">Has well over 100K stars on GitHub</li>
            <li className="classLi">Is maintained by Facebook</li>
            <li className="classLi">Powers thousands of enterprise apps, including mobile apps</li>
        </ul>
    </main>
    )
}

